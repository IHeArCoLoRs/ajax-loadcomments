<?php include("dbh.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Document</title>
    <style type="text/css">
        .center {
            text-align: center;
        }
        #comments {
            background-color: #000;
            min-height: 25px;
            color: white;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            var commentCount = 4;
            $("button").click(function(){
                commentCount += 4;
                $("#comments").load("load-comments.php", {
                    commentNewCount: commentCount
                });
            });
        });
    </script>
</head>
<body>
    

<div class="container" style="display: none;">
    <div id="comments" class="row">
        <?php 
            $sql = "SELECT * FROM comments LIMIT 4";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) > 0){
                while($row = mysqli_fetch_assoc($result)){
                    echo "<p>";
                    echo $row['author'] . "<br>";
                    echo $row['message'] . "<hr><br>";
                    echo "</p>";
                }
            } else {
                echo "<p> There are no comments. </p>";
            }    
        ?>
    </div>
    <br>
    <div class="col-md-12" align="center">
        <button class="btn btn-primary" style="width: 180px; margin: auto;">Show More Comments</button>
    </div>
</div>




    

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>